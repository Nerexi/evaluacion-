package com.example.dell.faccipmperezmurillonerexiapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Resultado extends AppCompatActivity {

    TextView Rcedula,Rnombre,Rapellido,Rciudad,Rfecha,Rfacultad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        Rcedula = (TextView) findViewById(R.id.textCr);
        Rnombre = (TextView) findViewById(R.id.textNr);
        Rapellido = (TextView) findViewById(R.id.textAr);
        Rciudad = (TextView) findViewById(R.id.textCn);
        Rfecha = (TextView) findViewById(R.id.textF);
        Rfacultad= (TextView) findViewById(R.id.textFl);

        Bundle bundle = this.getIntent().getExtras();
        Rcedula.setText(bundle.getString("ced"));
        Rnombre.setText(bundle.getString("nom"));
        Rapellido.setText(bundle.getString("apelli"));
        Rciudad.setText(bundle.getString("ciud"));
        Rfecha.setText(bundle.getString("fech"));
        Rfacultad.setText(bundle.getString("facul"));

    }
}
