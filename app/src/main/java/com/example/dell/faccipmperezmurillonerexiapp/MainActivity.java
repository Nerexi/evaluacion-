package com.example.dell.faccipmperezmurillonerexiapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText Cedula ,Nombre,Apellido,Ciudad,Fecha,Facultad;
    Button  botonIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Cedula=(EditText)findViewById(R.id.editCedula);
        Nombre=(EditText)findViewById(R.id.editName);
        Apellido=(EditText)findViewById(R.id.editApellido);
        Ciudad=(EditText)findViewById(R.id.editCiudad);
        Fecha=(EditText)findViewById(R.id.editFecha);
       Facultad=(EditText)findViewById(R.id.editFacultad);

       botonIngresar= (Button)findViewById(R.id.btnIngresar);

       botonIngresar.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(MainActivity.this,Resultado.class);
               Bundle bundle = new Bundle();

               bundle.putString("ced",Cedula.getText().toString());
               bundle.putString("nom",Nombre.getText().toString());
               bundle.putString("apelli",Apellido.getText().toString());
               bundle.putString("ciud",Ciudad.getText().toString());
               bundle.putString("fech",Fecha.getText().toString());
               bundle.putString("Facul",Facultad.getText().toString());


               intent.putExtras(bundle);

               Toast toast = Toast.makeText(getApplicationContext(), "Volver a ingresar datos regrese  a la pantalla anterior", Toast.LENGTH_LONG);
               toast.show();

               startActivity(intent);



           }


       });
       }
    }

